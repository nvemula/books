The app uses AFNetworking for networking calls.

The view Controllers for various screens are:  

AllBooksController - Books, 
ADDBookController - Add Book,  
EditBook Controller - Edit Book, 
Book Detail - Show details of book, 

Description : 

The AllBooksController is a tableView controller and shows the list of books. Users can add a new book by tapping the ‘+’ button. Swiping from right to left on a table view cell shows the delete button. 

The AddBookController contains a form to add a book and is presented modally when ‘+’ button is tapped by a user. When the form is filled and Save is tapped, the form is submitted to the server using AFNetworking.

In the AllBooksController, when a user taps on a book, the detail view i.e BookDetail View controller is shown. The book object is passed from the tableViewController and the view is updated.

When the user taps on Edit Book, the EditBookController is presented modally and the book object is passed to prefill the form. AFNetworking is used to send the update request to the server. I considered using FXForms for editing but didn’t use it as the fields were text only and FXForms was designed for mostly registration forms.




