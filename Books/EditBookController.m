//
//  ViewController.m
//  Books
//
//  Created by Nayan Vemula on 6/9/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//


// Implementation of View controller to edit a book
#import "EditBookController.h"
#import "FXForms.h"
#import "AFNetworking.h"
#import "Book.h"



@interface EditBookController ()

@property (weak, nonatomic) IBOutlet UITextField *bookTitle;

@property (weak, nonatomic) IBOutlet UITextField *authorsField;

@property (weak, nonatomic) IBOutlet UITextField *publishersField;

@property (weak, nonatomic) IBOutlet UITextField *categoriesField;

@end

@implementation EditBookController

@synthesize book;




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Update the view
    
    // Set the book title
    self.bookTitle.text = self.book.title;
    
    // Set the authors name
    self.authorsField.text = self.book.author;
    
    // Set the categories
    self.categoriesField.text = self.book.categories;
    
    // Set the publishers
    self.publishersField.text = self.book.publisher;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTapped:(id)sender {
    
    // Called on tapping Done in modal
    
    // Dismiss the modal
    [self dismissViewControllerAnimated:YES completion:nil];
  
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    // Dismiss the alert on tapping OK
    
    if(alertView.tag == 1){
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)saveBook:(id)sender {
    
    // Called on tapping Save in Edit book
    
    // If form is empty
    if(self.bookTitle.text.length==0 && self.authorsField.text.length==0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Book title and authors field are empty"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
    
    }
    
    else{
        
        // Send a request to update book
        [self updateABook];
    }
}

-(void)updateABook
{
    // Create a dict to send params
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject: self.bookTitle.text forKey:@"title"];
    [params setObject: self.authorsField.text forKey:@"author"];
    [params setObject: self.categoriesField.text forKey:@"categories"];
    [params setObject: self.publishersField.text forKey:@"publisher"];

    
    // Setup AFnetworking
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // Build the URL
    NSString *prolificURL = @"http://prolific-interview.herokuapp.com/5577a81d2bf01f0009700bf8";
    NSString *update = [prolificURL stringByAppendingString: self.book.url];

    [manager PUT:update parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Dismiss the modal if successful and use delegates to pass data to edit
        [self dismissViewControllerAnimated:YES completion:^{}];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // Show alert if it wasn't succesful
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"This book cannot be edited at this time"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        alert.tag = 3;
        [alert show];
    }];
    
}

@end
