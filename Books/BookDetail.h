//
//  BookDetail.h
//  Books
//
//  Created by Nayan Vemula on 6/13/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//

// Header file for View Controller when a book is tapped on Table view

#import <UIKit/UIKit.h>
#import "Book.h"
#import "EditBookController.h"


@interface BookDetail : UIViewController

// Selected book object from a list of books in tableview

@property (strong, nonatomic) Book *book;


@end

