//
//  BookDetail.m
//  
//
//  Created by Nayan Vemula on 6/13/15.
//
//


// Implementation of Book detail View Controller after a book is selected in table View

// Import all the header files
#import "BookDetail.h"
#import "Book.h"
#import "AFNetworking.h"
#import "EditBookController.h"

@interface BookDetail ()

// Label to display book Title
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;

// Label to display authors
@property (weak, nonatomic) IBOutlet UILabel *authors;

// Label to display publishers
@property (weak, nonatomic) IBOutlet UILabel *publishers;

// Label to display categories
@property (weak, nonatomic) IBOutlet UILabel *categories;

// Label to display lastChecked
@property (weak, nonatomic) IBOutlet UILabel *lastChecked;


@end

@implementation BookDetail

@synthesize book;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

-(void) viewWillAppear:(BOOL)animated {
    
    // Called before view appears
    [super viewWillAppear:animated];
    
    // Get the selected book passed through segue
    [self getBook];

    // Update the view
    [self updateVIew];
    }

-(void)updateVIew {
    // Set the bookTitle label
    self.bookTitle.text = book.title;
    
    // Set the authors label
    self.authors.text = book.author;
    
    
    if(![book.publisher  isEqual: @"(null)"]){
        
        // If there is a publisher

        self.publishers.text = book.publisher;
    }
    else {
        
        // No publisher
        
        self.publishers.text = @"";
        
        
    }
    
    if(![book.categories  isEqual: @"(null)"]){
        
        // If there are book categories
        
        self.categories.text = book.categories;
    }
    else {
        
        // If no categories
        
        self.categories.text = @"";
        
        
    }
    
    if(![book.lastCheckedOut  isEqual: @"(null)"] && ![book.lastCheckedOutBy  isEqual: @"(null)"]){
        
        // If the book was last checked out
        
        NSString *formattedDate = [self dateFormatter: self.book.lastCheckedOut];
        
        if(![formattedDate  isEqual: @""] && ![formattedDate  isEqual: @"(null)"]){
            
            // Change the date format
            
            NSString *lastCS = [NSString stringWithFormat:@"%@ on %@", self.book.lastCheckedOutBy, formattedDate];
            
            
            self.lastChecked.text = lastCS;
        }
        
        else {
            
            // If the book was not checked out at all set empty
            
            self.lastChecked.text = @"";
            
        }
    }
    
    else {
        
        // If the book was not checked out at all set empty
        
        self.lastChecked.text = @"";
        
    }



}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)checkoutBook:(id)sender {
    
    // Display alert on tapping Checkout
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Checkout"
                                                    message:@"Enter your name"
                                                   delegate:self
                                          cancelButtonTitle:@"Done"
                                          otherButtonTitles:@"Cancel", nil];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 0){
        
    // Update lastCheckout on tapping OK

    if([alertView textFieldAtIndex:0].text.length >0 ){
        
        self.book.lastCheckedOutBy = [alertView textFieldAtIndex:0].text;
        self.lastChecked.text = self.book.lastCheckedOutBy;
        [self updateCheckOut];
     }

    }


}


-(void)updateCheckOut
{
    
    // Put lastCheckedout data on the server
    
    // Create a dict of parameters to be sent
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject: self.book.lastCheckedOutBy forKey:@"lastCheckedOutBy"];

    // Setup AFNetworking manager
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // Build the URL
    NSString *prolificURL = @"http://prolific-interview.herokuapp.com/5577a81d2bf01f0009700bf8";
    NSString *update = [prolificURL stringByAppendingString:self.book.url];
    
    [manager PUT:update parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // If successful update the book
        
        self.book.lastCheckedOut = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"lastCheckedOut"]];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Pass book data to Edit form on tapping Edit book
    
    if ([[segue identifier] isEqualToString:@"gotoEdit"])
    {
        // Call the navigation controller
        UINavigationController *nav = [segue destinationViewController];
        
        // Pass it to the top viewcontroller in navigation controller stack
        EditBookController *details = (EditBookController *)nav.topViewController;
        
        // Set the data i.e book
        details.book = self.book;
    }
    
    
    
}

-(NSString *) dateFormatter:(NSString *) dateString
{
    // Create a formatter object
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the date format matching the one that is coming in the JSON
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    

    NSDate *date  = [dateFormatter dateFromString:dateString];

    // Set the desired date format
    [dateFormatter setDateFormat:@"MMMM d, yyyy h:mm a"];
    
    // Convert the date object to the desired string
    NSString *newDate = [dateFormatter stringFromDate:date];
    
    // Return the new format
    return newDate;

}


- (IBAction)shareButtonTapped:(id)sender {
    
    // Called on tapping the Share button

    // Build the URL
    NSString *prolificURL = @"http://prolific-interview.herokuapp.com/5577a81d2bf01f0009700bf8";
    NSString *textToShare = [prolificURL stringByAppendingString:self.book.url];
    
    // Pass the URL
    NSArray *itemsToShare = @[textToShare];
    
    // Call the activity view controller i.e Share sheet
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    
    // Ignore some actions
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    
    // Show the viewcontroller
    [self presentViewController:activityVC animated:YES completion:nil];

}

-(void)getBook
{
    // Get the book from server by passing the URL
    
    // Setup AFNetworking manager
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // Build the URL
    NSString *prolificURL = @"http://prolific-interview.herokuapp.com/5577a81d2bf01f0009700bf8";
    NSString *bookURL = [prolificURL stringByAppendingString:self.book.url];
    
    // Send the request
    [manager GET:bookURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
            // Update the book object from the response
        
            self.book.title = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"title"]];
            self.book.author = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"author"]];
            self.book.bookid = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"id"]];
            self.book.lastCheckedOut = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"lastCheckedOut"]];
            self.book.lastCheckedOutBy = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"lastCheckedOutBy"]];
            self.book.categories = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"categories"]];
            
            self.book.publisher = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"publisher"]];
            
            self.book.url = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"url"]];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}





@end



