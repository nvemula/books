//
//  ViewController.m
//  Books
//
//  Created by Nayan Vemula on 6/9/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//


// Implementation of tableView Controller to show all the books

#import "AllBooksController.h"
#import "AFNetworking.h"
#import "Book.h"
#import "BookDetail.h"
#import "AddBookController.h"

@interface AllBooksController ()

@property (strong, nonatomic) NSMutableArray *booksArray;
@property (strong, nonatomic) IBOutlet UITableView *booksList;

@end

@implementation AllBooksController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    /*Get the books and populate tableview. Currently loads table everytime the view appears
      Can be updated by using delegates to get data from modal and adding to the list
    */
    self.booksArray = [[NSMutableArray alloc] init];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    
    [self getAllBooks];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Set to activate delete button when swiped from right to left on tableView cell
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Get the book selected
        Book *book = [self.booksArray objectAtIndex: indexPath.row ];

        // Setup AFnetworking
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        // Build the URL
        NSString *prolificURL = @"http://prolific-interview.herokuapp.com/5577a81d2bf01f0009700bf8";
        NSString *update = [prolificURL stringByAppendingString: book.url];
        
        [manager DELETE:update parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            // Remove the book from the list after succesffully deleting on server
            [self.booksArray removeObjectAtIndex:indexPath.row];
            
            // Delete tableview row
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
        
        
       

    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.booksArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Populate table View
    
    static NSString *CellIdentifier = @"BooksCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell=[[UITableViewCell alloc]initWithStyle:
              UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    Book *book = [self.booksArray objectAtIndex: indexPath.row ];

    
    cell.textLabel.text = book.title;
    cell.detailTextLabel.text= book.author;

    
    return cell;
}

-(void)getAllBooks
{
    // Get all the books from server
    
    // Setup AFNetworking
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // Build the URL
    [manager GET:@"http://prolific-interview.herokuapp.com/5577a81d2bf01f0009700bf8/books/?" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
            for(NSDictionary *item in responseObject) {
                
                // Loop and get the json objects
                Book *book = [[Book alloc]init];
                
                // Convert to book objects
                book.title = [NSString stringWithFormat:@"%@",[item objectForKey:@"title"]];
                book.author = [NSString stringWithFormat:@"%@",[item objectForKey:@"author"]];
                book.bookid = [NSString stringWithFormat:@"%@",[item objectForKey:@"id"]];
                book.lastCheckedOut = [NSString stringWithFormat:@"%@",[item objectForKey:@"lastCheckedOut"]];
                book.lastCheckedOutBy = [NSString stringWithFormat:@"%@",[item objectForKey:@"lastCheckedOutBy"]];
                book.categories = [NSString stringWithFormat:@"%@",[item objectForKey:@"categories"]];

                book.publisher = [NSString stringWithFormat:@"%@",[item objectForKey:@"publisher"]];

                book.url = [NSString stringWithFormat:@"%@",[item objectForKey:@"url"]];

                // Add books to the list
                [self.booksArray addObject:book];

    }
        
        // Reload the tableview
        [self.booksList reloadData];
        

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}


- (IBAction)deleteAllBooks:(id)sender {
    
    // Delete all the books
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete All"
                                                    message:@"Are you sure you want to delete all the books?"
                                                   delegate:self
                                          cancelButtonTitle:@"Yes"
                                          otherButtonTitles:@"No", nil];
    alert.tag = 1;
    
    [alert show];

    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    // Delete all the books when Yes is tapped
    if(alertView.tag == 1){
        if(buttonIndex == 0){
            [self deleteAllBooks];
            
        }
    }
    
    
    
}


-(void)deleteAllBooks
{
    // Delete all books on server
    
    // Setup AFNetworking
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // URL
    NSString *prolificURL = @"http://prolific-interview.herokuapp.com/5577a81d2bf01f0009700bf8/clean";
    
    [manager DELETE:prolificURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Update the tableView
        self.booksArray = nil;
        [self.tableView reloadData];
        
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // Show alert if deletion wasn't successfull
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"This action is not possible at this time"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 2;
        [alert show];
        
        NSLog(@"Error: %@", error);
    }];

    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Pass the book to detail view on selecting a tableView cell
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if ([[segue identifier] isEqualToString:@"gotoBook"])
    {
        // Pass the book
        BookDetail *details = segue.destinationViewController;
        details.book = [self.booksArray objectAtIndex:indexPath.row];
    }
    
    

}

@end
