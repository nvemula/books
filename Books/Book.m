//
//  Book.m
//  
//
//  Created by Nayan Vemula on 6/13/15.
//
//

// Book object used to convert JSON objects to Foundation

#import <Foundation/Foundation.h>

#import "Book.h"

@implementation Book

@synthesize title;
@synthesize author;
@synthesize categories;
@synthesize bookid;
@synthesize publisher;
@synthesize url;
@synthesize lastCheckedOut;
@synthesize lastCheckedOutBy;



@end