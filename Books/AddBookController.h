//
//  AddBookController.h
//  Books
//
//  Created by Nayan Vemula on 6/10/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//

// Header file for the view controller to add a new book

#import <UIKit/UIKit.h>
#import "FXForms.h"
#import "Book.h"

@interface AddBookController : UIViewController<UIAlertViewDelegate>

@property (weak, nonatomic) Book *book;


@end