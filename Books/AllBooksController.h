//
//  ViewController.h
//  Books
//
//  Created by Nayan Vemula on 6/9/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//


// Header file for the table view to display all the books
#import <UIKit/UIKit.h>

@interface AllBooksController : UITableViewController <UITableViewDataSource,UIAlertViewDelegate>


@end

