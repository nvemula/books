//
//  Book.h
//  Books
//
//  Created by Nayan Vemula on 6/13/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Book : NSObject

@property(nonatomic, retain)  NSString *title;
@property(nonatomic, retain)  NSString *author;
@property(nonatomic, retain)  NSString *publisher;
@property(nonatomic, retain)  NSString *url;
@property(nonatomic, retain)  NSString *bookid;
@property(nonatomic, retain)  NSString *categories;
@property(nonatomic, retain)  NSString *lastCheckedOut;
@property(nonatomic, retain)  NSString *lastCheckedOutBy;

@end