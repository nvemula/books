//
//  ViewController.m
//  Books
//
//  Created by Nayan Vemula on 6/9/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//

// Implementation of the view controller to add a book

#import "AddBookController.h"
#import "FXForms.h"
#import "AFNetworking.h"


@protocol ModalDelegate
-(void)doneEditingBook:(Book *)book;
@end

@interface AddBookController ()

@property (weak, nonatomic) IBOutlet UITextField *bookTitle;

@property (weak, nonatomic) IBOutlet UITextField *authorsField;

@property (weak, nonatomic) IBOutlet UITextField *publishersField;

@property (weak, nonatomic) IBOutlet UITextField *categoriesField;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;


@end

@implementation AddBookController

@synthesize book;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTapped:(id)sender {
    
    // Called on tapping the Done button
    
    if(self.bookTitle.text.length==0 && self.authorsField.text.length==0 &&
       self.categoriesField.text.length==0 && self.publishersField.text.length==0){
        
        // If the form is empty, dismiss the modal
        
        [self dismissViewControllerAnimated:YES completion:nil];

    }
    else{
      
        // Show an alert as there is some data in the form
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Done"
                                                  message:@"Are you sure you don't want to add this book?"
                                                  delegate:self
                                                  cancelButtonTitle:@"Yes"
                                                  otherButtonTitles:@"No", nil];
        alert.tag = 1;
       [alert show];
   }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 1){
      if(buttonIndex == 0){
          
        // Yes is tapped so close the modal
        
        [self dismissViewControllerAnimated:YES completion:nil];

      }
    }
   
    
    
}
- (IBAction)submitTapped:(id)sender {
    
    // Called on tapping Submit
    
    if(self.bookTitle.text.length==0 && self.authorsField.text.length==0){
        
        // Form is empty so display an alert
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Book title and authors field are empty"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 2;
        [alert show];
    
    }
    
    else{
    
        // Data is present in the form
        
        // Call add new book method to POST data to server
        [self addNewBook];
        
        // Dismiss the modal
        [self dismissViewControllerAnimated:YES completion:nil];

    }

    
    
}

-(void)addNewBook
{
    // Create dict to send the parameters along with the request
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject: self.bookTitle.text forKey:@"title"];
    [params setObject: self.authorsField.text forKey:@"author"];
    [params setObject: self.categoriesField.text forKey:@"categories"];
    [params setObject: self.publishersField.text forKey:@"publisher"];

    // Setup AFNetworking manager
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // URL to send request
    NSString *prolificURL = @"http://prolific-interview.herokuapp.com/5577a81d2bf01f0009700bf8/books/";
    
    [manager POST:prolificURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Succesful
        
        // Update the book object
        self.book.title = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"title"]];
        self.book.author = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"author"]];
        self.book.categories = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"categories"]];
        self.book.publisher = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"publisher"]];
        self.book.url = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"url"]];
        self.book.bookid = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"id"]];


    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
         // Display an alert if the POST was not successful on the server
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"This book cannot be added at this time"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        alert.tag = 3;
        [alert show];
            
        NSLog(@"Error: %@", error);
    }];
    
}



@end
