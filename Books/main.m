//
//  main.m
//  Books
//
//  Created by Nayan Vemula on 6/9/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
