//
//  AppDelegate.h
//  Books
//
//  Created by Nayan Vemula on 6/9/15.
//  Copyright (c) 2015 Happy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

